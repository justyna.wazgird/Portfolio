var gulp = require('gulp');
var yarn = require('gulp-yarn');
var browserSync = require('browser-sync').create();
var sass = require('gulp-sass');
var rename = require('gulp-rename');
var autoprefixer = require('gulp-autoprefixer');

gulp.task('sass', function() {
  return gulp.src('public/sass/main.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(sass({
      outputStyle: 'compressed'
    }))
    .pipe(autoprefixer({
      browsers: ['> 0%'],
      cascade: false
    }))
    .pipe(rename({
      basename: 'style',
      suffix: '.min'
    }))
    .pipe(gulp.dest('public/css/'))
    .pipe(browserSync.stream());
});

gulp.task('yarn', function() {
  return gulp.src(['./package.json', './yarn.lock'])
    .pipe(gulp.dest('./public'))
    .pipe(yarn({
      production: true
    }));
});


gulp.task('build', function() {
  return gulp.src('public/sass/main.scss')
    .pipe(autoprefixer({
      browsers: ['> 0%'],
      cascade: false
    }))
    .pipe(sass().on('error', sass.logError))
    .pipe(sass({
      outputStyle: 'compressed'
    }))
    .pipe(rename({
      basename: 'style',
      suffix: '.min'
    }))
    .pipe(gulp.dest('public/css/'))
});


gulp.task('serve', ['sass'], function() {
  browserSync.init({
    server: "./public"
  });

  gulp.watch('public/sass/**/*.scss', ['sass']);
  gulp.watch('public/js/script.js').on('change', browserSync.reload);
  gulp.watch("public/index.html").on('change', browserSync.reload);
});

gulp.task('default', ['yarn', 'serve']);
