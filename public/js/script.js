// masonry filtering
var $grid = $('.projects-list').imagesLoaded( function() {
  $grid.isotope({
    itemSelector: '.project'
  });

  // update fancybox rel depending on isotope filtering
  $grid.on( 'arrangeComplete', function( event, filteredItems ) {
    var currentFilter = $(this).data('current-filter');
    $(filteredItems).each(function() {
      $($(this)[0].element).find("a").attr('rel', currentFilter);
    })
  });
});

$('.filters-button-group').on( 'click', 'button', function() {
  var filterValue = $( this ).attr('data-filter');
  $(".projects-list").data("current-filter", filterValue);
  $grid.isotope({
    filter: filterValue
  });
});

$('.button-group').each( function( i, buttonGroup ) {
  var $buttonGroup = $( buttonGroup );
  $buttonGroup.on( 'click', 'button', function() {
    $buttonGroup.find('.is-checked').removeClass('is-checked');
    $( this ).addClass('is-checked');
  });
});

$(document).ready(function() {
// fancybox init
  $(".fancybox").fancybox();
// wow.js init
  new WOW().init();
});
